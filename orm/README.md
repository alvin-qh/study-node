Study orm libs
===

## Install dependencies

```bash
# install orm lib
$ npm install --save sequelize

# install db driver
$ npm install --save mysql

# datetime support
$ npm install --save moment

# mocha support
$ npm install --global mocha

# should support
$ npm install --save-dev should
```

## Document
[http://docs.sequelizejs.com/en/latest/docs/getting-started/]