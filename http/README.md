Study io libs
===

## Install dependencies

```shell
# core lib
$ npm install --save jade
$ npm install --save ejs
$ npm install --save nunjucks

# server side jquery lib
$ npm install --save cheerio
```
