Study lodash lib
===

## Install lodash

```shell
$ npm install --save lodash
```

## Use lodash

CommonJS

```javascript
import * as _ from "lodash";
```

NodeJS

```javascript
const _  = require("lodash");
```

## Documents


[Lodash document](https://lodash.com/docs)