Study node.js
===

## Setup

### Install node.js

```bash
$ brew install node
```

## Using npm China proxy

```bash
$ npm config set registry https://registry.npm.taobao.org
```

