export function add(a, b) {
    return a + b;
}

export default class Person {
    constructor(name, age, gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }
}

