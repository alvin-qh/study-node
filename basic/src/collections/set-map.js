export function setOf(...args) {
    return new Set(args);
}

export function mapOf(...keyValue) {
    return new Map(keyValue);
}