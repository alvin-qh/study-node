Study mocha test lib
===

## Install mocha

```shell
$ npm install --global mocha
```

## Install dependencies

```shell
$ npm install --save-dev should
```
